package example.test

import bus.EventBusPublisher
import bus.FeatureDataContainer
import bus.FeatureEventScope
import core.Action
import core.BaseState
import core.FinalStateMachine
import core.StateMachine

class EventFinalStateMachine(
    defaultStateCallBack: ((BaseState<MachineEventPoint, EventBusAction>) -> Unit)? = null
) : FinalStateMachine<EventBusAction, MachineEventPoint>(defaultStateCallBack) {

    private var eventBusPublisher: EventBusPublisher<FeatureDataContainer, FeatureEventScope>? = null

    override fun eventOccurred(action: EventBusAction): FinalStateMachine<EventBusAction, MachineEventPoint> {
        val stateMachine = super.eventOccurred(action)
        val scopes = currentState.value.listScope
        val date = currentState.value.dataContainer
        this.eventBusPublisher?.let { publisher ->
            scopes.forEach { scope ->
                publisher.overrideDate(date, scope)
            }
        }
        return stateMachine
    }

    override fun addEventBus(
        publisher: EventBusPublisher<FeatureDataContainer, FeatureEventScope>
    ): FinalStateMachine<EventBusAction, MachineEventPoint> {
        this.eventBusPublisher = publisher
        return this
    }

    companion object {
        fun build(
            callBack: ((BaseState<MachineEventPoint, EventBusAction>) -> Unit)? = null,
            init: StateMachine<EventBusAction, MachineEventPoint>.() -> Unit
        ): StateMachine<EventBusAction, MachineEventPoint> {
            val stateMachine = EventFinalStateMachine(callBack)
            stateMachine.init()
            return stateMachine
        }

        fun  build(
            graph: Map<MachineEventPoint, List<Pair<MachineEventPoint, EventBusAction>>>,
            callBack: ((BaseState<MachineEventPoint, EventBusAction>) -> Unit)? = null
        ): StateMachine<EventBusAction, MachineEventPoint> {
            return build(callBack) {
                graph.forEach {
                    state(it.key) {
                        it.value.forEach { edgeValue ->
                            edge(edgeValue.first) { action ->
                                edgeValue.second == action
                            }
                        }
                    }
                }
            }
        }
    }
}