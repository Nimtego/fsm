package example.test

import bus.FeatureDataContainer
import bus.FeatureEventScope
import core.FeaturePointState

abstract class MachineEventPoint(
    val dataContainer: FeatureDataContainer,
    val listScope: List<FeatureEventScope>
) : FeaturePointState()
data class FirstState(
    val data: FeatureDataContainer,
    val scopes: List<FeatureEventScope>
) : MachineEventPoint(data, scopes)
data class SecondState(
    val data: FeatureDataContainer,
    val scopes: List<FeatureEventScope>
) : MachineEventPoint(data, scopes)
data class ThirdState(
    val data: FeatureDataContainer,
    val scopes: List<FeatureEventScope>
) : MachineEventPoint(data, scopes)
