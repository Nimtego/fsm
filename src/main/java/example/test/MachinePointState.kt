package example.test

import core.FeaturePointState

sealed class MachinePointState : FeaturePointState()
object WaitState : MachinePointState()
object WorkState : MachinePointState()
object DoubleWorkState : MachinePointState()
object SleepState : MachinePointState()
object AbortState : MachinePointState()