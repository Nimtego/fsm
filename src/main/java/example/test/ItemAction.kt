package example.test

import core.FeatureAction

sealed class ItemAction : FeatureAction()
object Dress : ItemAction()
object PutInBag : ItemAction()
object Throw : ItemAction()
object PutInStorage : ItemAction()