package example.test

import core.FeatureAction

sealed class MachineWorkAction : FeatureAction()
object WaitAction : MachineWorkAction()
object WorkAction : MachineWorkAction()
object DoubleWorkAction : MachineWorkAction()
object SleepAction : MachineWorkAction()
object AbortAction : MachineWorkAction()