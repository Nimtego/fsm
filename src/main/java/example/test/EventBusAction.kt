package example.test

import core.FeatureAction

sealed class EventBusAction : FeatureAction()
object ToOne : EventBusAction()
object ToTwo : EventBusAction()
object ToThird : EventBusAction()