package example.test

import core.FeaturePointState

sealed class ItemState : FeaturePointState()
object InMap : ItemState()
object InBag : ItemState()
object PutOn : ItemState()
object InStorage : ItemState()