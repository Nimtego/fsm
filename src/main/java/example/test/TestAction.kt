package example.test

import core.FeatureAction

sealed class TestAction : FeatureAction()
object GoToOne : TestAction()
object GoToTwo : TestAction()
object GoToThird : TestAction()