package example

import bus.*
import core.Action
import core.BaseState
import core.FinalStateMachine
import core.StateMachine
import example.test.*

object Solution {
    @JvmStatic
    fun main(args: Array<String>) {
//        test()
//        testGraph()
//        eventBusTest()
        testItems()
        //testEventBusFsm()
    }

    private fun test() {
        val fsm: StateMachine<TestAction, Int> = FinalStateMachine.build(::handleState) {
            state(1) {
                edge(2) { it is GoToTwo }
                edge(3) { it is GoToThird }
                edge(1) { it is GoToOne }
            }
            state(2) {
                edge(1) { it is GoToOne }
                edge(3) { it is GoToThird }
                edge(2) { it is GoToTwo }
            }
            state(3) {
                edge(1) { it is GoToOne }
                edge(2) { it is GoToTwo }
                edge(3) { it is GoToThird }
            }
        }

        fsm.initialize()
            .eventOccurred(GoToTwo)
            .eventOccurred(GoToOne)
            .eventOccurred(GoToThird)
            .eventOccurred(GoToTwo)
            .eventOccurred(GoToTwo)
            .eventOccurred(GoToThird)
    }

    private fun testGraph() {
        val graph = mapOf(
            WaitState to listOf(
                WorkState to WorkAction,
                SleepState to SleepAction,
                DoubleWorkState to DoubleWorkAction
            ),
            WorkState to listOf(
                DoubleWorkState to DoubleWorkAction,
                SleepState to SleepAction
            ),
            SleepState to listOf(
                WorkState to WorkAction,
                AbortState to AbortAction
            ),
            AbortState to listOf(
                WaitState to WaitAction
            ),
            DoubleWorkState to listOf(
                AbortState to AbortAction
            )
        )
        FinalStateMachine.build(graph, ::handleState)
            .initialize()
            .eventOccurred(DoubleWorkAction)
            .eventOccurred(WorkAction)
            .eventOccurred(DoubleWorkAction)
            .eventOccurred(AbortAction)
            .eventOccurred(WorkAction)
            .eventOccurred(SleepAction)
            .eventOccurred(WaitAction)
            .eventOccurred(WaitAction)
            .eventOccurred(WaitAction)
    }

    fun testItems() {
        val graph = mapOf(
            InMap to listOf(
                InBag to PutInBag
            ),
            InStorage to listOf(
                InBag to PutInBag
            ),
            InBag to listOf(
                InMap to Throw,
                PutOn to Dress,
                InStorage to PutInStorage
            ),
            PutOn to listOf(
                InBag to PutInBag
            )
        )
        FinalStateMachine.build(graph, ::handleState)
            .initialize()
            .eventOccurred(Throw)
            .eventOccurred(Dress)
            .eventOccurred(PutInBag)
            .eventOccurred(PutInStorage)
            .eventOccurred(PutInBag)
            .eventOccurred(Dress)
            .eventOccurred(PutInBag)
            .eventOccurred(Throw)
    }

    fun testEventBusFsm() {
        val first = FirstState(IdContainer(1), listOf(FirstScope))
        val second = SecondState(IdContainer(2), listOf(SecondScope))
        val third = ThirdState(MessageContainer("234"), listOf(ThirdScope))

        val graph = mapOf(
            first to listOf(second to ToTwo, third to ToThird),
            second to listOf(third to ToThird),
            third to listOf(first to ToOne, second to ToTwo)
        )

        val eventBus: EventBus<FeatureDataContainer, FeatureEventScope> = BaseEventBus()
        val publisher: EventBusPublisher<FeatureDataContainer, FeatureEventScope> = eventBus
        val subscriber: EventBusSubscriber<FeatureDataContainer, FeatureEventScope> = eventBus

        subscriber.behaviorDate(FirstScope).subscribe {
            println()
            when(it) {
                is IdContainer -> println("${it.id} in FirstScope")
                is MessageContainer -> println("${it.message} in FirstScope")
                else -> println("Unknown in FirstScope")
            }
        }
        subscriber.behaviorDate(SecondScope).subscribe {
            println()
            when(it) {
                is IdContainer -> println("${it.id} in SecondScope")
                else -> println("Unknown in SecondScope")
            }
        }
        subscriber.behaviorDate(ThirdScope).subscribe {
            println()
            when(it) {
                is IdContainer -> println("${it.id} in ThirdScope")
                is MessageContainer -> println("${it.message} in ThirdScope")
                else -> println("Unknown in ThirdScope")
            }
        }


        EventFinalStateMachine.build(graph, ::handleState)
            .addEventBus(publisher)
            .initialize()
            .eventOccurred(ToTwo)
            .eventOccurred(ToThird)
            .eventOccurred(ToTwo)
            .eventOccurred(ToThird)
            .eventOccurred(ToOne)
    }

    fun eventBusTest() {
        val eventBus: EventBus<FeatureDataContainer, FeatureEventScope> = BaseEventBus()
        val publisher: EventBusPublisher<FeatureDataContainer, FeatureEventScope> = eventBus
        val subscriber: EventBusSubscriber<FeatureDataContainer, FeatureEventScope> = eventBus
        subscriber.behaviorDate(FirstScope).subscribe {
            when(it) {
                is MessageContainer -> println(it.message)
                is IdContainer -> println(it.id)
                else -> println("Unknown")
            }
        }
        subscriber.behaviorDate(SecondScope).subscribe {
            when(it) {
                is IdContainer -> println(it.id)
                else -> println("Unknown")
            }
        }

        publisher.overrideDate(MessageContainer("123"), FirstScope)
        publisher.overrideDate(MessageContainer("123"), SecondScope)
        publisher.overrideDate(IdContainer(345), FirstScope)
        publisher.overrideDate(IdContainer(345), SecondScope)
        publisher.overrideDate(DefContainer, FirstScope, SecondScope)
        publisher.overrideDate(MessageContainer("FT"), ThirdScope)

        subscriber.behaviorDate(ThirdScope).subscribe {
            when(it) {
                is MessageContainer -> println(it.message)
                is IdContainer -> println(it.id)
                else -> println("Unknown")
            }
        }
    }

    private fun <V, A : Action, S : BaseState<V, A>> handleState(state: S) {
        println("Set value to ${state.value}")
    }
}