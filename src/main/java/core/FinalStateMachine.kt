package core

import bus.EventBusPublisher
import bus.FeatureDataContainer
import bus.FeatureEventScope

open class FinalStateMachine<A : Action, V>(
        private val defaultStateCallBack: ((BaseState<V, A>) -> Unit)? = null
) : StateMachine<A, V> {

    protected lateinit var currentState: BaseState<V, A>
    private var defaultValue: V? = null
    private val stateList = mutableListOf<BaseState<V, A>>()
    private var eventBusPublisher: EventBusPublisher<FeatureDataContainer, FeatureEventScope>? = null

    override fun initialize(): FinalStateMachine<A, V> {
        defaultValue?.let {
            currentState = getStateByName(it)
            currentState.enterState()
        }
        return this
    }

    override fun eventOccurred(action: A): FinalStateMachine<A, V> {
        val edge = currentState.getEdgeForEvent(action)
        if (edge != null) {
            val newState = edge.enterEdge { getStateByName(it) }
            newState.enterState()
            currentState = newState
        } else {
            currentState.enterState()
        }
        defaultStateCallBack?.invoke(currentState)
        return this
    }

    override fun state(value: V, init: BaseState<V, A>.() -> Unit) {
        if (defaultValue == null) {
            this.defaultValue = value
        }
        val state = BaseState<V, A>(value)
        state.init()
        stateList.add(state)
    }

    override fun addEventBus(publisher: EventBusPublisher<FeatureDataContainer, FeatureEventScope>): FinalStateMachine<A, V> {
        this.eventBusPublisher = publisher
        return this
    }

    private fun getStateByName(value: V): BaseState<V, A> {
        return stateList.firstOrNull {
            it.value == value
        } ?: throw NoSuchElementException(value.toString())
    }

    companion object {
        fun <A : Action, V> build(
            callBack: ((BaseState<V, A>) -> Unit)? = null,
            init: StateMachine<A, V>.() -> Unit
        ): StateMachine<A, V> {
            val stateMachine = FinalStateMachine(callBack)
            stateMachine.init()
            return stateMachine
        }

        fun <A : Action, V> build(
                graph: Map<V, List<Pair<V, A>>>,
                callBack: ((BaseState<V, A>) -> Unit)? = null
        ): StateMachine<A, V> {
            return build(callBack) {
                graph.forEach {
                    state(it.key) {
                        it.value.forEach { edgeValue ->
                            edge(edgeValue.first) { action ->
                                edgeValue.second == action
                            }
                        }
                    }
                }
            }
        }
    }
}
