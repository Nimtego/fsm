package core

abstract class FeatureAction : Action()

sealed class Action
object GoUpEvent: Action()
object GoDownEvent: Action()
object HaltEvent: Action()
object TopLimitHitEvent: Action()
object BottomLimitHitEvent: Action()