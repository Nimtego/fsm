package core

open class BaseState<V, A: Action>(val value: V) {
    private val edgeList = mutableListOf<BaseEdge<V, A>>()
    private val stateEnterAction = mutableListOf<(BaseState<V, A>) -> Unit>()

    fun edge(destination: V, init: (A) -> Boolean) {
        val edge = BaseEdge<V, A>(value, destination)
        edge.eventHandler = init
        edgeList.add(edge)
    }

    fun action(action: (BaseState<V, A>) -> Unit) {
        stateEnterAction.add(action)
    }

    fun enterState() {
        stateEnterAction.forEach { it(this) }
    }

    fun getEdgeForEvent(action: A): BaseEdge<V, A>? {
        return edgeList.firstOrNull { it.canHandleEvent(action) }
    }
}