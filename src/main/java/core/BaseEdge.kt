package core

open class BaseEdge<V, A: Action>(
        val currentState: V,
        val targetState: V
) {
    lateinit var eventHandler: (A) -> Boolean

    private val actionList = mutableListOf<(BaseEdge<V, A>) -> Unit>()

    fun action(action: (BaseEdge<V, A>) -> Unit) {
        actionList.add(action)
    }

    fun enterEdge(retrieveState: (V) -> BaseState<V, A>): BaseState<V, A> {
        actionList.forEach { it(this) }
        return retrieveState(targetState)
    }

    fun canHandleEvent(action: A): Boolean {
        return eventHandler(action)
    }
}