package core

import bus.EventBusPublisher
import bus.FeatureDataContainer
import bus.FeatureEventScope

interface StateMachine<A: Action, V> {
    fun initialize(): StateMachine<A, V>
    fun eventOccurred(action: A): StateMachine<A, V>
    fun state(value: V, init: BaseState<V, A>.() -> Unit)
    fun addEventBus(publisher: EventBusPublisher<FeatureDataContainer, FeatureEventScope>): StateMachine<A, V>
}