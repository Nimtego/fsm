package bus

import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

interface EventBusSubscriber<D: Any, S: EventScope> {
    fun getDate(scope: S): D?
    fun behaviorDate(scope: S): BehaviorSubject<D>
    fun publishDate(scope: S): PublishSubject<D>
    fun setReed(scope: S, data: D)
}