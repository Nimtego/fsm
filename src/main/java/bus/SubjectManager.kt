package bus

import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

interface SubjectManager<D: Any> {
    fun getCurrentElement(): D?
    fun overrideCurrentElement(date: D)
    fun behaviorCurrentElement(): BehaviorSubject<D>
    fun publishCurrentElement(): PublishSubject<D>
    fun clear()
}