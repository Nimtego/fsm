package bus

interface EventBusPublisher<D: Any, S: EventScope> {
    fun overrideDate(date: D, scope: S)
    fun overrideDate(date: D, vararg scope: S)
    fun clear(scope: S)
}