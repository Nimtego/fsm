package bus

abstract class FeatureDataContainer : DataContainer()

sealed class DataContainer

object DefContainer : FeatureDataContainer()
data class IdContainer(val id: Int) : FeatureDataContainer()
data class MessageContainer(val message: String) : FeatureDataContainer()