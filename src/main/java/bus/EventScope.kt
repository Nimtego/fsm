package bus

abstract class FeatureEventScope : EventScope()

sealed class EventScope

object FirstScope : FeatureEventScope()
object SecondScope : FeatureEventScope()
object ThirdScope : FeatureEventScope()