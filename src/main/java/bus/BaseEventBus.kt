package bus

import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject


class BaseEventBus<D : FeatureDataContainer, S : FeatureEventScope> : EventBus<D, S> {

    private val values = HashMap<S, SubjectManager<D>>()
    private val subscribers = HashMap<S, Pair<Int, Int>>()

    private fun createIfNotExist(scope: S) {
        if (!values.containsKey(scope)) {
            values[scope] = EventBusSubjectManager()
            subscribers[scope] = Pair(0, 0)
        }
    }

    override fun getDate(scope: S): D? {
        createIfNotExist(scope)
        return values[scope]?.getCurrentElement()
    }

    override fun overrideDate(date: D, scope: S) {
        createIfNotExist(scope)
        values[scope]?.overrideCurrentElement(date)
    }

    override fun overrideDate(date: D, vararg scope: S) {
        scope.forEach { overrideDate(date, it) }
    }

    override fun behaviorDate(scope: S): BehaviorSubject<D> {
        createIfNotExist(scope)
        subscribers[scope]?.let { pair ->
            subscribers[scope] = pair.copy(
                    first = pair.first.inc()
            )
        }
        return values[scope]!!.behaviorCurrentElement()
    }

    override fun publishDate(scope: S): PublishSubject<D> {
        createIfNotExist(scope)
        subscribers[scope]?.let { pair ->
            subscribers[scope] = pair.copy(
                    second = pair.second.inc()
            )
        }
        return values[scope]!!.publishCurrentElement()
    }

    override fun clear(scope: S) {
        if (values.contains(scope)) {
            values.remove(scope)
        }
    }

    override fun setReed(scope: S, data: D) {
        overrideDate(data, scope)
    }
}