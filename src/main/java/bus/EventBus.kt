package bus

interface EventBus<D: Any, S: EventScope> : EventBusPublisher<D, S>, EventBusSubscriber<D, S>