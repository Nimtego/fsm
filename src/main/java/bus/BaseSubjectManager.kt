package bus

import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

abstract class BaseSubjectManager<D : Any> : SubjectManager<D> {

    protected var currentItem: D? = null
    protected val behavior: BehaviorSubject<D> = BehaviorSubject.create()
    protected val publish: PublishSubject<D> = PublishSubject.create()

    override fun getCurrentElement(): D? {
        return this.currentItem
    }

    override fun overrideCurrentElement(date: D) {
        this.currentItem = date
        this.currentItem?.let {
            this.behavior.onNext(it)
            this.publish.onNext(it)
        }
    }

    override fun behaviorCurrentElement(): BehaviorSubject<D> {
        return this.behavior
    }

    override fun publishCurrentElement(): PublishSubject<D> {
        return this.publish
    }

    override fun clear() {}
}